terraform {
  required_providers {
    hcloud = {
      source  = "hetznercloud/hcloud"
      version = "1.41.0"
    }
  }
}

provider "hcloud" {
  token = var.hcloud_token
}

resource "hcloud_network" "cluster-network" {
  name     = "cluster-network"
  ip_range = "192.168.0.0/16"
}

resource "hcloud_network_subnet" "cluster-subnet" {
  type         = "cloud"
  network_id   = hcloud_network.cluster-network.id
  network_zone = "eu-central"
  ip_range     = "192.168.0.0/24"
}

resource "hcloud_server" "heads" {
  for_each    = {for head in range(1, 1+var.heads_number) : head => head}
  name        = format("head%s", each.value)
  server_type = var.heads_server_type
  image       = var.image
  location    = var.location

  public_net {
    ipv4_enabled = var.individual_public_ipv4_addresses
  }
  network {
    network_id = hcloud_network.cluster-network.id
    ip         = format("192.168.0.%s", each.value+3)
  }

  ssh_keys = var.ssh_keys

  labels = {
    "function" : "head"
  }

  # **Note**: the depends_on is important when directly attaching the
  # server to a network. Otherwise Terraform will attempt to create
  # server and sub-network in parallel. This may result in the server
  # creation failing randomly.
  depends_on = [
    hcloud_network_subnet.cluster-subnet
  ]
}

resource "hcloud_server" "worker" {
  for_each    = {for head in range(1, 1+var.worker_number) : head => head}
  name        = format("worker%s", each.value)
  server_type = var.worker_server_type
  image       = var.image
  location    = var.location

  public_net {
    ipv4_enabled = var.individual_public_ipv4_addresses
  }
  network {
    network_id = hcloud_network.cluster-network.id
    ip         = format("192.168.0.%s", each.value+3+var.heads_number)
  }

  ssh_keys = var.ssh_keys

  labels = {
    "function" : "worker"
  }

  # **Note**: the depends_on is important when directly attaching the
  # server to a network. Otherwise Terraform will attempt to create
  # server and sub-network in parallel. This may result in the server
  # creation failing randomly.
  depends_on = [
    hcloud_network_subnet.cluster-subnet
  ]
}

resource "hcloud_load_balancer" "heads_load_balancer" {
  count = var.setup_loadbalancer ? 1 : 0
  name               = "heads-load-balancer"
  load_balancer_type = "lb11"
  location           = var.location
}

resource "hcloud_load_balancer_network" "heads_load_balancer_network" {
  count = var.setup_loadbalancer ? 1 : 0
  load_balancer_id = hcloud_load_balancer.heads_load_balancer[1].id
  network_id       = hcloud_network.cluster-network.id
  ip               = "192.168.0.2"
}

resource "hcloud_load_balancer_target" "heads_load_balancer_target" {
  count = var.setup_loadbalancer ? 1 : 0
  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.heads_load_balancer[1].id
  label_selector   = "function=head"
  use_private_ip   = true
  depends_on       = [hcloud_load_balancer_network.heads_load_balancer_network[1]]
}

resource "hcloud_load_balancer_service" "heads_load_balancer_service" {
  count = var.setup_loadbalancer ? 1 : 0
  load_balancer_id = hcloud_load_balancer.heads_load_balancer[1].id
  protocol         = "tcp"
  listen_port      = 443
  destination_port = 443
}

resource "hcloud_load_balancer" "worker_load_balancer" {
  count = var.setup_loadbalancer ? 1 : 0
  name               = "worker-load-balancer"
  load_balancer_type = "lb11"
  location           = var.location
}

resource "hcloud_load_balancer_network" "worker_load_balancer_network" {
  count = var.setup_loadbalancer ? 1 : 0
  load_balancer_id = hcloud_load_balancer.worker_load_balancer[1].id
  network_id       = hcloud_network.cluster-network.id
  ip               = "192.168.0.3"
}

resource "hcloud_load_balancer_target" "worker_load_balancer_target" {
  count = var.setup_loadbalancer ? 1 : 0
  type             = "label_selector"
  load_balancer_id = hcloud_load_balancer.worker_load_balancer[1].id
  label_selector   = "function=worker"
  use_private_ip   = true
  depends_on       = [hcloud_load_balancer_network.worker_load_balancer_network]
}

resource "hcloud_load_balancer_service" "worker_load_balancer_service" {
  count = var.setup_loadbalancer ? 1 : 0
  load_balancer_id = hcloud_load_balancer.worker_load_balancer[1].id
  protocol         = "tcp"
  listen_port      = 443
  destination_port = 443
}
