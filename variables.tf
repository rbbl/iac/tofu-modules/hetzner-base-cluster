variable "hcloud_token" {
  type = string
}

variable "location" {
  type    = string
  default = "nbg1"
}

variable "image" {
  type    = string
  default = "centos-stream-8"
}

variable "ssh_keys" {
  type = list(string)
}

variable "heads_number" {
  type        = number
  default     = 3
  description = "must be an odd number according to kubernetes spec"
}

variable "heads_server_type" {
  type    = string
  default = "cx21"
}

variable "worker_number" {
  type    = number
  default = 2
}

variable "worker_server_type" {
  type    = string
  default = "cpx31"
}

variable "individual_public_ipv4_addresses" {
  type = bool
  default = false
}

variable "setup_loadbalancer" {
  type = bool
  default = true
}